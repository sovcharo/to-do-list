import React from 'react';
import './App.css';
import TaskList from './components/list/TaskList';

function App() {
  return (
    <div>
      <h2>From zero to drug lord.</h2>
      <h3>10 easy steps.</h3>
      <TaskList />
    </div>
  );
}

export default App;
