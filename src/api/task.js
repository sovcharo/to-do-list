import '../mock/taskListJson'
import taskList from '../mock/taskListJson';

async function getTaskList(){
    return taskList;
}

export default getTaskList;