import React from 'react';

const Task = ({task, changeTask, ...rest}) => {
    console.log(rest);
    return (
        
        <div id = {task.order}>
                <p>
                    <input type="checkbox" onChange = {() => changeTask(task.order, task.done)} /*onChange = {setFinished(!done)}*//> {task.order}.  
                    <span className = {task.done ? "strikethrough" : ""}>
                        {task.name}
                    </span>
                </p>
                <p>
                    <em>{task.desc}</em>
                </p>
            </div>
            );
}

export default Task;