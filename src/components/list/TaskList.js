import React, { useEffect, useState } from 'react';
import getTaskList from '../../api/task';
import Task from './Task';

const TaskList = () => {
    const [tasks, setTasks] = useState([]);

    useEffect(() => {
        async function fetchList() {
            const taskList = await getTaskList();
            setTasks(taskList);
        }
        fetchList();
    }, []);

    function changeTask (order, done) {
        console.log('click click')
        tasks.map(item => {
            if(item.order === order) {
                item.done = !done;
            }

            return item
        });
        setTasks(tasks);
    }
    console.log({tasks});
    return (
        <div> 
        {tasks.map(task => {
            return(
                <div onClick={() => changeTask(task.order, task.done)}> 
                <h1 >click me </h1>
                {console.log(task.done)}
            <p>{task.done ? 'done' : 'not done'}</p>
                </div>
            )
            // return (
            //     <Task task = {task} isItDone={task.done}  changeTask = {changeTask} />
            //     );
        })}
         </div>
    );
}

export default TaskList;