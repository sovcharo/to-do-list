const taskList =  [
    {   
        "order" : 1,
        "name" : "Stalk the bridges", 
        "desc" : "Usually there are a lot of drug addicts. Create a drug base by stealing from passed out users.", 
        "done" : true
    },
    {
        "order" : 2,
        "name" : "Integrate yourself in the system", 
        "desc" : "After a year of drug accumulation you should be able to start selling.", 
        "done" : true
    },
    {
        "order" : 3,
        "name" : "Buy aspirin and sell it at the local clubs as the next big thing.", 
        "desc" : "Always spend some time turning it into powder. Customers should receive the luxurious package.", 
        "done" : false
    },
    {
        "order" : 4,
        "name" : "After accumulating some money you can start visiting the local orphanage.",
        "desc" : "You should always think in perspective. Expansion will surely be upon you soon enough and you will need strong and able bodies.", 
        "done" : false
    },
    {
        "order" : 5,
        "name" : "Police protection", 
        "desc" : "This is something you should have done in the beginning. But hey I know, no money. Now you have it and it is time for you to have your own area.", 
        "done" : false
    },
    {
        "order" : 6,
        "name" : "Build an information network", 
        "desc" : "Other lords don't appreciate your company in their regions. You should know who hates you.", 
        "done" : false
    },
    {
        "order" : 7,
        "name" : "Ensure your safety", 
        "desc" : "Spread rumors how dangerous you are. Hire a person who will pose as yourself.", 
        "done" : false
    },
    {
        "order" : 8,
        "name" : "Purchase weapons from the black market", 
        "desc" : "You always liked weapons and you started the whole thing because you wanted to buy yourself something big and black which causes bruises when fired.", 
        "done" : false
    },
    {
        "order" : 9,
        "name" : "Follow you dreams", 
        "desc" : "Now you have enough money to start your own weapon business.", 
        "done" : false
    },
    {
        "order" : 10,
        "name" : "Hire your first biological weapons specialist.", 
        "desc" : "You always had issues with your ego. Now you can really work on a large scale.", 
        "done" : false
    }
];

export default taskList;